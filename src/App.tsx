import React, { Component } from 'react';
import './App.css';
import { ContractType } from './models/enums/ContractType';
import { ContractTypeSelector } from './components/ContractTypeSelector';
import { SalarySelector } from './components/SalarySelector';
import { Salary } from './models/Salary';
import { SalaryCosts } from './components/SalaryCosts';
import { Button } from '@material-ui/core';
import calculateUoPSalary from './salary/UoPSalary';
import { B2BCost } from './models/B2BCost';
import { B2BCostsList } from './components/b2bCost/B2BCostsList';
import calculateB2BSalary from './salary/B2BSalary';

interface State {
  salary: Salary
  contractType: ContractType,
  b2bCosts: B2BCost[]
}

interface Props {

}

class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { 
      salary: {
        netto: 0,
        brutto: 0,
        superBrutto: 0,
        bruttoCosts: new Map(),
        superBruttoCosts: new Map()
      },
      contractType: ContractType.UoP,
      b2bCosts: []
    }
  }

 private setContractType = (event: React.ChangeEvent<HTMLInputElement>) => {
    const contractType = ContractType[(event.target as HTMLInputElement).value as keyof typeof ContractType]
    this.setState({
      contractType: contractType,
      salary: {
        netto: 0,
        brutto: 0,
        superBrutto: 0,
        bruttoCosts: new Map(),
        superBruttoCosts: new Map()
      },
      b2bCosts: []
    })
  }

  private changeSalary = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newSalary = parseInt(event.target.value)
    this.setState({
      salary: {
        netto: 0,
        brutto: newSalary,
        superBrutto: 0,
        bruttoCosts: this.state.salary.bruttoCosts,
        superBruttoCosts: this.state.salary.bruttoCosts
      }
    })
  }

  private calculateSalary = () => {
    let newSalary: number = this.state.salary.brutto;

    let calculatedSalary: Salary;

    switch (this.state.contractType) {
      case ContractType.B2B: calculatedSalary = calculateB2BSalary(newSalary, this.state.b2bCosts); break;
      case ContractType.UoP: calculatedSalary = calculateUoPSalary(newSalary); break;
      case ContractType.UZ: calculatedSalary = calculateUoPSalary(newSalary); break;   
      default: throw Error("Invalid contract type");
  }
    this.setState({salary: calculatedSalary})
  }

  private onCostsSave = (cost: B2BCost) => {
    this.setState({b2bCosts: [...this.state.b2bCosts, cost]})
  }
  

  render() {
    return (
    <div>
      <ContractTypeSelector contractType={this.state.contractType} onChange={this.setContractType}/>
      <B2BCostsList b2bCosts={this.state.b2bCosts} onSave={this.onCostsSave} visible={this.state.contractType === ContractType.B2B}/>
      <br/><SalarySelector netto={this.state.salary.netto} brutto={this.state.salary.brutto} superBrutto={this.state.salary.superBrutto} handleChange={this.changeSalary}/>
      <br/><Button onClick={this.calculateSalary}>Oblicz</Button>
      <br/><SalaryCosts bruttoCosts={this.state.salary.bruttoCosts} superBruttoCosts={this.state.salary.superBruttoCosts}/>
    </div>
    );
  }
}

export default App;
