import { ContractType } from '../models/enums/ContractType'
import { FormControl, FormControlLabel, RadioGroup, Radio } from "@material-ui/core";
import React from 'react';

interface Props {
    contractType: ContractType
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

export const ContractTypeSelector = (props: Props) => {
    return (
        <FormControl>
            <RadioGroup value={props.contractType} onChange={props.onChange} row>
                <FormControlLabel value={ContractType.B2B} control={<Radio />} label="B2B"/>
                <FormControlLabel value={ContractType.UoP} control={<Radio />} label="UoP"/>
                <FormControlLabel value={ContractType.UZ} control={<Radio />} label="UZ"/>
            </RadioGroup>
        </FormControl>
    );
}
    
