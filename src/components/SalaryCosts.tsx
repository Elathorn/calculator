import { TableHead, Table, TableCell, TableRow, TableBody, Paper, makeStyles, Theme, createStyles } from "@material-ui/core";
import React from 'react';

interface Props {
    bruttoCosts: Map<String, number>
    superBruttoCosts: Map<String, number>
}

interface Cost {
    desc: String
    value: number
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginTop: theme.spacing(3),
      overflowX: 'auto',
    },
    table: {
        float: 'left',
        width: '45%',
        border: '2px solid Tomato',
    },
    lastRow: {
        border: '1px Black'
    }
  }),
);

export const SalaryCosts = (props: Props) => {
    const bruttoCosts: Cost[] = []
    props.bruttoCosts.forEach((v, k) => {
        bruttoCosts.push(({
            desc: k,
            value: v
        }))
    })

    const superBruttoCosts: Cost[] = []
    props.superBruttoCosts.forEach((v, k) => {
        superBruttoCosts.push(({
            desc: k,
            value: v
        }))
    })

    const classes = useStyles()

    return (
        <Paper>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Koszty brutto</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {bruttoCosts.map((entry) => {
                        return (
                        <TableRow>
                            <TableCell>{entry.desc}</TableCell>
                            <TableCell>{entry.value}</TableCell>
                        </TableRow>
                        )
                    })}
                    <TableRow className={classes.lastRow}>
                        <TableCell>Łączny koszt</TableCell>
                        <TableCell>{bruttoCosts.reduce((a,b) => a+b.value, 0).toFixed(2)}</TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell>Koszty superbrutto</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {superBruttoCosts.map((entry) => {
                        return (
                        <TableRow>
                            <TableCell>{entry.desc}</TableCell>
                            <TableCell>{entry.value}</TableCell>
                        </TableRow>
                        )
                    })}
                    <TableRow className={classes.lastRow}>
                        <TableCell>Łączny koszt</TableCell>
                        <TableCell>{superBruttoCosts.reduce((a,b) => a+b.value, 0).toFixed(2)}</TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </Paper>
    );
}