import { TextField } from "@material-ui/core";
import React from "react";


interface Props {
    netto: number
    brutto: number
    superBrutto: number
    handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

export const SalarySelector = (props: Props) => {
    return (
        <form>
            <TextField
            name="netto"
            label="Zarobki Netto"
            value={props.netto}
            //type="number"
            margin="normal"
            InputProps={{
                readOnly: true,
            }}
            />
            <TextField
            name="brutto"
            label="Zarobki Brutto"
            value={props.brutto}
            onChange={props.handleChange}
            type="number"
            margin="normal"
            />
            <TextField
            name="superBrutto"
            label="Zarobki SuperBrutto"
            value={props.superBrutto}
            //type="number"
            margin="normal"
            InputProps={{
                readOnly: true,
            }}
            />
        </form>
    );
}