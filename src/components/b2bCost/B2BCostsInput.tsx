import { B2BCost } from "../../models/B2BCost"
import { TextField, Button } from "@material-ui/core"
import React from "react"

interface Props {
    onSave: (cost: B2BCost) => void
}

interface State {
    cost: B2BCost
}

const vatRates = [0, 5, 8, 23]

export const B2BCostsInput = (props: Props) => {
    
    const [value, setValue] = React.useState<B2BCost>({
        name: 'Komputer',
        netto: 2000,
        vat: 23
    })

    const changeName = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newName = event.target.value
        setValue((previousState) => ({
            name: newName,
            netto: value.netto,
            vat: value.vat
        }))
    }
    
    const changeNetto = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newNetto = parseInt(event.target.value)
        setValue((previousState) => ({
            name: value.name,
            netto: newNetto,
            vat: value.vat
        }))
    }


    const changeVat = (event: React.ChangeEvent<HTMLInputElement>) => {
        const newVat = parseInt(event.target.value)
        setValue((previousState) => ({
            name: value.name,
            netto: value.netto,
            vat: newVat
        }))
    }

    return (
        <form>
            <TextField required label="Nazwa" value={value.name} onChange={changeName}/>
            <TextField required label="Koszt netto" value={value.netto} type="number" onChange={changeNetto}/>
            <TextField required select label="Stawka VAT" value={value.vat} onChange={changeVat}>
                {vatRates.map(rate => (<option key={rate} value={rate}>{rate}</option>))}
            </TextField>
            <Button onClick={() => props.onSave(value)}>Dodaj</Button>
        </form>
    )
}