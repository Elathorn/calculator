import { B2BCost } from "../../models/B2BCost";
import { TableCell, Table, TableHead, TableRow, TableBody } from "@material-ui/core";
import React from "react";
import { B2BCostsInput } from "./B2BCostsInput";

interface Props {
    b2bCosts: B2BCost[],
    visible: boolean,
    onSave: (cost: B2BCost) => void
}


export const B2BCostsList = (props: Props) => {
    if (!props.visible) {
        return null;
    }
    return (
    <form>
    <Table>
    <TableHead>
        <TableRow>
            <TableCell>Nazwa</TableCell>
            <TableCell>Koszt netto</TableCell>
            <TableCell>VAT</TableCell>
        </TableRow>
    </TableHead>
    <TableBody>
        {props.b2bCosts.map((entry) => {
            return (
            <TableRow>
                <TableCell>{entry.name}</TableCell>
                <TableCell>{entry.netto}</TableCell>
                <TableCell>{entry.vat}</TableCell>
            </TableRow>
            )
        })}
    </TableBody>
    </Table>
    <B2BCostsInput onSave={props.onSave}/>
    </form>
    )

}