export interface B2BCost {
    name: String
    netto: number
    vat: number
}