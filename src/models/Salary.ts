export interface Salary {
  netto: number
  brutto: number
  superBrutto: number
  bruttoCosts: Map<String, number>
  superBruttoCosts: Map<String, number>
}