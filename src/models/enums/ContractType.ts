export enum ContractType {
    UoP = 'UoP',
    B2B = 'B2B',
    UZ = 'UZ'
}