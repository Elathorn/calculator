import { B2BCost } from "../models/B2BCost";
import { Salary } from "../models/Salary";

const lineIncomeTaxPercent = 0.19
const vatRate = 0.23

export default function calculateB2BSalary(newSalary: number, costs: B2BCost[]): Salary {

    const [netto, bruttoCosts] = calculateNettoFromBrutto(newSalary, costs);
    const [superBrutto, superBruttoCosts] = calculateSuperBruttoFromBrutto(newSalary, costs);

    return ({
        netto: netto,
        brutto: newSalary,
        superBrutto: superBrutto,
        bruttoCosts: bruttoCosts,
        superBruttoCosts: superBruttoCosts
    })
}

function calculateNettoFromBrutto(salary: number, costs: B2BCost[]): [number, Map<String, number>] {
    const netto = salary - salary * lineIncomeTaxPercent - costs.reduce((sum, cost) => sum + cost.netto * lineIncomeTaxPercent, 0)
    const bruttoCosts = new Map(costs.map(c => [`${c.name} (${lineIncomeTaxPercent}% dochodowy)`, c.netto * lineIncomeTaxPercent / 100]))
    return [netto, bruttoCosts]
}

function calculateSuperBruttoFromBrutto(salary: number, costs: B2BCost[]): [number, Map<String, number>] {
    const vatBase = salary + salary * vatRate
    const superBrutto = vatBase - costs.reduce((sum, cost) => sum + cost.netto * cost.vat / 100, 0)
    const superBruttoCosts = new Map(costs.map(c => [`${c.name} (${c.vat}% VAT)`, c.netto * c.vat / 100]))
    return [superBrutto, superBruttoCosts]
}