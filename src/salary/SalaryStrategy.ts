import { ContractType } from "../models/enums/ContractType";
import calculateUoPSalary from "./UoPSalary";
import { Salary } from "../models/Salary";

const emptySalary: Salary = ({
    netto: 0,
    brutto: 0,
    superBrutto: 0,
    bruttoCosts: new Map(),
    superBruttoCosts: new Map()
})

export function calculateNewSalary(contractType: ContractType, newSalary: number): Salary {
    switch (contractType) {
        case ContractType.B2B: return emptySalary;
        case ContractType.UoP: return calculateUoPSalary(newSalary)
        case ContractType.UZ: return emptySalary;    
    }
}