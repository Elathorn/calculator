import { Salary } from "../models/Salary";

export default function calcNewSalaries(newSalary: number): Salary {
    return ({
        netto: 0,//calcNettoFromBrutto(newSalary),
        brutto: newSalary,
        superBrutto: 0,//calcSuperBruttoFromBrutto(newSalary)
        bruttoCosts: new Map(),
        superBruttoCosts: new Map()
    })
}

/*function calcNettoFromBrutto(newSalary: number): number {
    
}*/