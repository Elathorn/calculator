import { Salary } from "../models/Salary";

function round(number: number): number { //todo do sth 
    return Number(number.toFixed(2))
}

export default function calculateUoPSalary(newSalary: number): Salary {
    
    const [netto, bruttoCosts] = calcNettoFromBrutto(newSalary);
    const [superBrutto, superBruttoCosts] = calcSuperBruttoFromBrutto(newSalary)
    return ({
        netto: netto,
        brutto: newSalary,
        superBrutto: superBrutto,
        bruttoCosts: bruttoCosts,
        superBruttoCosts: superBruttoCosts
    })
}

const retirmentContributionPercent = 0.0976
const annuitiesContributionPercent = 0.015
const sickContributionPercent = 0.0245
const socialContributionPercent = retirmentContributionPercent + annuitiesContributionPercent + sickContributionPercent

const healthcareInsurancePercent = 0.09

const incomeTaxRatePercent = 0.18
const taxFree = 46.33
const incomeCost = 111.25
const healthcareContributionPercent = 0.0775

function calcNettoFromBrutto(brutto: number): [number, Map<String, number>] {
    let costs = new Map<String, number>()

    const socialContributionSum = round(brutto * socialContributionPercent)
    costs.set(`Ubezpieczenie emerytalne (${retirmentContributionPercent*100}%)`, round(brutto * retirmentContributionPercent))
    costs.set(`Ubezpieczenie rentowe (${annuitiesContributionPercent*100}%)`, round(brutto * annuitiesContributionPercent))
    costs.set(`Ubezpieczenie chorobowe (${sickContributionPercent*100}%)`, round(brutto * sickContributionPercent))

    const afterSocialContribution = brutto - socialContributionSum
    const healthcareInsurance = round(afterSocialContribution * healthcareInsurancePercent)
    costs.set(`Składak na ubezpieczenie zdrowotne (${healthcareInsurancePercent*100}%)`, healthcareInsurance)

    let incomeTaxBase = afterSocialContribution - incomeCost
    let incomeTax = round(incomeTaxBase * incomeTaxRatePercent - taxFree)

    let healthcareContribution = round(afterSocialContribution * healthcareContributionPercent)
    incomeTax = Math.round(incomeTax - healthcareContribution)
    costs.set(`Zaliczka na podatek dochodowy`, incomeTax)
    
    const netto = brutto - socialContributionSum - healthcareInsurance - incomeTax

    return [netto, costs]
}

function calcSuperBruttoFromBrutto(brutto: number): [number, Map<String, number>] {
    let costs = new Map<String, number>()

    const annuitiesContributionPercent = 0.065
    const accidentContributionPercent = 0.0018
    const workFundContributionPercent = 0.0245
    const fgspContributionPercent = 0.01

    costs.set(`Ubezpieczenie emerytalne (${retirmentContributionPercent*100}%)`, round(brutto * retirmentContributionPercent))
    costs.set(`Ubezpieczenie rentowe (${annuitiesContributionPercent*100}%)`, round(brutto * annuitiesContributionPercent))
    costs.set(`Ubezpieczenie wypadkowe (${accidentContributionPercent*100}%)`, round(brutto * accidentContributionPercent))
    costs.set(`Składka na Fundusz Pracy (${workFundContributionPercent*100}%)`, round(brutto * workFundContributionPercent))
    costs.set(`Składka na FGŚP (${fgspContributionPercent*100}%)`, round(brutto * fgspContributionPercent))

    const contributionPercentsSum = retirmentContributionPercent + annuitiesContributionPercent + accidentContributionPercent + workFundContributionPercent + fgspContributionPercent
    const superBrutto = brutto + (brutto * contributionPercentsSum)

    return [superBrutto, costs]
}
